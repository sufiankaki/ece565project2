%Generation of a smooth image using 9x9 averaging filter
filter = (1/81)*[1 1 1 1 1 1 1 1 1; 
    1 1 1 1 1 1 1 1 1; 
    1 1 1 1 1 1 1 1 1; 
    1 1 1 1 1 1 1 1 1; 
    1 1 1 1 1 1 1 1 1;
    1 1 1 1 1 1 1 1 1; 
    1 1 1 1 1 1 1 1 1; 
    1 1 1 1 1 1 1 1 1; 
    1 1 1 1 1 1 1 1 1];
f=imread('Figure1.tif');
f=im2double(f);
out=conv2(f,filter);
figure;
imshow(f);
title('Original Image');

figure;
imshow(out);
title('Averaged Image');

%Obtaining binary image by thresholding
binaryimg = im2bw(out, 0.55);
figure;
imshow(binaryimg);
title('Binary Image');


%Extracting outer boundary
[bound, L,N] = bwboundaries(binaryimg);
bound1 = bound{1};
mat_size = size(binaryimg);
outerboundary = zeros(mat_size);
for i = 1:length(bound1)
    outerboundary(bound1(i,1), bound1(i,2)) = 1;
end
figure;
imshow(outerboundary);
title('Outer Boundary');


%Subsampling
[s, sunit] = bsubsamp(bound1, 50);
mat_size = size(binaryimg);
sampledboundary = zeros(mat_size);
for i = 1:length(s)
    sampledboundary(s(i,1), s(i,2)) = 1;
end
figure;
imshow(sampledboundary);
title('Sampled Boundary');


%Connecting as a polygon
x=s(:,1);
y=s(:,2);
c = connectpoly(x,y);
polygon = zeros(mat_size);
for i = 1:length(c)
    polygon(c(i,1), c(i,2)) = 1;
end
figure;
imshow(polygon);
title('Polygon form');

fch = fchcode(c,8);
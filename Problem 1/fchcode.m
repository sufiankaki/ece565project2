function [c] = fchcode(b, CONN)

sb=circshift(b,[-1 0]);
delta=sb-b;

% convert dy,dx pairs to scalar indexes
%   --------------------------------------
%   | deltay | deltax | code | (base-3)+1 |
%   |-------------------------------------|
%   |    0   |   +1   |   0  |      9     | 
%   |    0   |   -1   |   4  |      3     | 
%   |   +1   |   +1   |   1  |     11     | 
%   |   +1   |   -1   |   3  |      5     | 
%   |   +1   |    0   |   2  |      8     | 
%   |   -1   |    0   |   6  |      4     | 
%   |   -1   |   +1   |   7  |      7     |  
%   |   -1   |   -1   |   5  |      1     | 
%   ---------------------------------------

temp8=2*delta(:,1)+3*delta(:,2)+6;
codemap8([1 3 4 5 7 8 9 11])=[5 4 6 3 7 2 0 1];
code8 = codemap8(temp8);

% convert dy,dx pairs to scalar indexes 
%   --------------------------------------
%   | deltay | deltax | code | (base-3)+1 |
%   |-------------------------------------|
%   |    0   |   +1   |   0  |      9     | 
%   |    0   |   -1   |   2  |      3     | 
%   |   +1   |   +1   |   1  |     11     | 
%   |   +1   |   -1   |   2  |      5     | 
%   |   +1   |    0   |   1  |      8     | 
%   |   -1   |    0   |   3  |      4     | 
%   |   -1   |   +1   |   0  |      7     |  
%   |   -1   |   -1   |   3  |      1     | 
%   ---------------------------------------
temp4=2*delta(:,1)+3*delta(:,2)+6;
codemap4([1 3 4 5 7 8 9 11])=[3 2 3 2 0 1 0 1];
code4 = codemap4(temp4);



diffcode8 = zeros(1, length(code8));
for i = 1:(length(code8)-1)
    if(code8(i+1) - code8(i) < 0)
        diffcode8(i) = 8-(code8(i) - code8(i+1));
    else
        diffcode8(i) = code8(i+1) - code8(i);
    end
end

diffcode4 = zeros(1, length(code4));
for i = 1:(length(code4)-1)
    if(code4(i+1) - code4(i) < 0)
        diffcode4(i) = 4-(code4(i) - code4(i+1));
    else
        diffcode8(i) = code4(i+1) - code4(i);
    end
end


mm8 = minimuminteger(code8);
mm4 = minimuminteger(code4);

diffcode_mm8 = zeros(1, length(mm8));
for i = 1:(length(mm8)-1)
    if(mm8(i+1) - mm8(i) < 0)
        diffcode_mm8(i) = 8-(mm8(i) - mm8(i+1));
    else
        diffcode_mm8(i) = mm8(i+1) - mm8(i);
    end
end

diffcode_mm4 = zeros(1, length(mm4));
for i = 1:(length(mm4)-1)
    if(mm4(i+1) - mm4(i) < 0)
        diffcode_mm4(i) = 4-(mm4(i) - mm4(i+1));
    else
        diffcode_mm4(i) = mm4(i+1) - mm4(i);
    end
end


if (CONN == 8) 
    c.fcc = code8;
    c.diff = diffcode8;
    c.mm = mm8;
    c.diffmm = diffcode_mm8;
    c.x0y0 = [b(1,2), b(1,1)];
elseif (CONN == 4) 
    c.fcc = code4;
    c.diff = diffcode4;
    c.mm = mm4;
    c.diffmm = diffcode_mm4;
    c.x0y0 = [b(1,2), b(1,1)];
end